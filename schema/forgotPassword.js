var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;

module.exports = exports = new Schema({
    user: {
        type: ObjectId,
        ref:'auth.User',
        required: true
    },
    slug: {
        type: String,
        required: true
    },
    expirationDate: {
        type: Date,
        required: true
    },
    used: {
        type:Boolean,
        required:true,
        default:false
    }
}, {});

