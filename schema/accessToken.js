var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;

module.exports = exports = new Schema({
    isActive: {
        type: Boolean,
        required: true,
        default: true
    },
    user: {
        type: ObjectId,
        ref:'auth.User',
        required: true
    },
    token: {
        type: String,
        required: true
    },
    expirationDate: {
        type: Date,
        required: true
    }


}, {});

