module.exports = exports = {
	AccessToken: require("./accessToken"),
	Activation: require("./activation"),
	ForgotPassword: require("./forgotPassword"),
	User: require("./user"),
};
