var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    plugins = require('aio-mongoose-plugins');

var userSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: false
    },
}, {});

userSchema.plugin(plugins.AclObject);
userSchema.plugin(plugins.CreatedAndModified);
userSchema.plugin(plugins.ActiveAndDeleted);
module.exports = exports = userSchema;
