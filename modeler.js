var _ = require('lodash'),
    mongoose = require('mongoose'),
    schemalib = require('./schema'),
    async = require('async'),
    util = require('util'),
    path = require('path'),
    owl = require('owl-deepcopy'),
    config = require('annulet-config'),

    logger = config.configuration.logger;
_.mixin(require('annulet-util')
    .lodashMixins);
var makeSchemaCopy = function(schema) {
    //create clone of schema
    var newSchema = owl.deepCopy(schema);
    //fix callQueue arguments (borrowed from mongoose-schema-extend, even though we're not using it)
    //this prevents 'numAsyncPres of undefined' error
    //hack: there is probably a better way to fix callQueue arugments
    newSchema.callQueue.forEach(function(k) {
        var args = [];
        var ref = k[1];
        for (key in ref) {
            args.push(ref[key]);
        }
        return k[1] = args;
    });

    return newSchema;
};
module.exports = exports = {
    _db: {},
    models: {},
    setup: function(options, cb) {
        logger.warn('db auth setup called');
        //options:
        //{
        //  authDb: function or string describing auth db uri
        //}
        var dburi = ((function() {
            if (_.isFunction(options.authDb)) {
                return options.authDb();
            } else if (_.isString(options.authDb)) {
                return options.authDb;
            } else {
                throw new Error('authDb must be a string or a function');
            }
        }) || config.paths.authDb)();

        logger.silly('[auth connection] got dburi: ' + dburi);

        var db = this._db,
            models = this.models;
        db.connection = mongoose.createConnection(dburi);
        db.connection.on('error', function(err) {
            logger.error('[auth connection error] ' + util.inspect(err));
        });

        var addSchema = function(path, schema, hash) {
            _.each(_.keys(schema), function(schemaName) {
                logger.silly('[modeler] adding ' + schemaName);
                //todo: look at using _.getObjectPath instead
                var fullQual = path + ((path || '') == '' ? '' : '.') + schemaName;
                if (schema[schemaName] instanceof mongoose.Schema) {
                    //use a copy of the schema
                    hash[schemaName] = db.connection.model(fullQual, makeSchemaCopy(schema[schemaName]));
                } else {
                    //otherwise, treat as plain (container) object and recurse
                    hash[schemaName] = {};
                    addSchema(fullQual, schema[schemaName], hash[schemaName]);
                }
            });
        };

        logger.silly('[modeler] about to make schema into models');
        addSchema('', schemalib, models);

        cb(null);
    },
    db: function(options, cb) {
        //keep consistent with annulet-models
        //hack: for now, ignore options in auth models db call?
        cb(null, this.models);
    }
};
